import { assert, expect, should } from "chai";
import { describe, it } from "mocha";
import calc from "../src/calc.js";

describe("calc.js", () => {
    let myVar;
    before(() => {
        myVar = 0;
    }  );
    beforeEach(() => myVar++);
    afterEach(() => console.log(`myVar = ${myVar}`));

    it("can add numbers", () => {
        expect(calc.add(2, 2)).to.equal(4);
    }   );
    it("can subtract numbers", () => {
        assert(calc.subtract(5, 1) === 4);
        assert(calc.subtract(2, 3) === -1);
        // expect(calc.subtract(5, 1)).to.equal(4);
    }   );
    it("can multiply numbers", () => {
        should().exist(calc.multiply);
        expect(calc.multiply(3, 3)).to.equal(9);
    }   ); 
    it('can divide numbers', () => {
        expect(calc.divide(10, 2)).to.equal(5);
    }  );
    it("0 divison throws an error", () => {
        const err_msg = "Cannot divide by zero";
        expect(() => calc.divide(10, 0)).to.throw(err_msg);
    });
});


// it.skip is a way to skip a test. Set it to pending.