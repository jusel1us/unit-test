import express from 'express';
import calc from './calc.js';

const app = express();
const port = 3000;
const host = "localhost";

// Endpoint - GET http://localhost:3000/
app.get('/', (req, res) => { //req = request, res = response
    res.status(200).send('Hello World!'); // 200 = OK
}   );

// Endpoint - GET http://localhost:3000/add?a=2&b=3
app.get('/add', (req, res) => {
    const a = parseFloat(req.query.a);
    const b = parseFloat(req.query.b);
    const sum = calc.add(a, b);
    res.status(200).send(`${a} + ${b} = ${sum}`);
}  );

app.listen(port, host, () => {
    console.log(`Server running at http://${host}:${port}/`);
}   );