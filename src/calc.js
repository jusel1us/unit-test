/**
 * Adds two numbers together.
 * @param {number} a 
 * @param {number} b 
 * @returns {number}
 */
const add = (a, b) => a + b;

const subtract = (minuend, subtrahend) => {
    return minuend - subtrahend;
};

const multiply = (multiplier, multiplicant) => {
    return multiplier * multiplicant;
};

/**
 * 
 * @param {number} dividend
 * @param {number} divisor
 * @returns {number}
 * @throws {Error} Cannot divide by zero
 */
const divide = (dividend, divisor) => {
    if (divisor === 0) throw new Error("Cannot divide by zero");
    const fraction = dividend / divisor;
    return fraction;
}

export default { add, subtract, multiply, divide }